var Beer = require("../../models/beer");
var msg = '';

module.exports = {
	create: function(req, res, cb) {
		var dados = req.body; //recebe os dados json
		var model = new Beer(dados);
		model.save(function (err, data) {
			cb(err, data, res);
		});
	},
	retrieve: function(req, res, cb) {
		var query = {};
		Beer.find(query, function (err, data) {
			cb(err, data, res);
			res.end();
		});
	},
	findOne: function(req, res, cb){
		var id = req.params.id;
		var query = { _id: id };

		Beer.findOne(query, function (err, data) {
			cb(err, data, res);
			res.end();
		});
	},
	update: function(req, res, cb) {
		var id = req.params.id;
		var query = { _id: id };
		var mod = req.body;
		// var optional = {multi: true};
		Beer.update(query, mod, function (err, data) {
			cb(err, data, res);
		});
	},
	delete: function(req, res, cb) {
		var id = req.params.id;
		var query = { _id: id };

		Beer.remove(query, function (err, data) {
			cb(err, data, res);
		});
	}
};

// module.exportes = _beer;