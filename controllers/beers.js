var Beer = require("../models/beer");
var msg = '';

module.exports = {
	renderList: function(req, res, cb) {
		var message = 'Listagem Completa';
		var view = 'beers/list';
		var query = {};
		Beer.find(query,function (err, data) {
			cb(err, data, res, view, message);
			res.end();
		});
	},
	renderShow: function(req, res, cb) {
		var message = 'Detalhes da cerveja';
		var view = 'beers/show';
		var id = req.params.id;
		var query = {_id: id};
		Beer.findOne(query,function (err, data) {
			cb(err, data, res, view, message);
			res.end();
		});
	},
	renderCreate: function(req, res, cb) {
		var message = 'Adicionar Cerveja';
		var view = 'beers/create';
		cb(null, null, res, view, message);
	},
	renderEdit: function(req, res, cb) {
		var message = 'Editar Cerveja';
		var view = 'beers/edit';
		var id = req.params.id;
		var query = {_id: id};
		Beer.findOne(query,function (err, data) {
			cb(err, data, res, view, message);
			res.end();
		});
	},
	renderRemove: function(req, res, cb) {
		var message = 'Remover Cerveja';
		var view = 'beers/remove';
		var id = req.params.id;
		var query = {_id: id};
		Beer.findOne(query,function (err, data) {
			cb(err, data, res, view, message);
			res.end();
		});
	}
};

// module.exportes = _beer;